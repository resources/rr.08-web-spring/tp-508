drop table if exists etudiant;

create table etudiant (
    id int not null,
    prenom varchar(255) not null,
    nom varchar(255) not null,
    age int not null,
    groupe varchar(1) not null
);
