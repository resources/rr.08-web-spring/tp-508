package fr.but3.tp508;

import lombok.Data;
import lombok.ToString;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.GeneratedValue;

@Entity
@ToString
@Data
public class Etudiant {
    @Id
    int id;
    String prenom;
    String nom;
    int age;
    String groupe;

}

