package fr.but3.tp508;

import java.util.List;
import java.util.Map;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

@RestController
@RequestMapping(value = "etudiant", produces={MediaType.APPLICATION_JSON_VALUE})
public class ControleurEtudiantRest {

    @Autowired
    private EtudiantRepository etudiantRepository;

    @GetMapping(produces={MediaType.APPLICATION_JSON_VALUE})
    List<Etudiant> getAllStudent() {
        return (List<Etudiant>) this.etudiantRepository.findAll();
    }

    @PostMapping(produces={MediaType.APPLICATION_JSON_VALUE})
    Etudiant saveStudent(@RequestBody Etudiant student) {
        return this.etudiantRepository.save(student);
    }

    /* #########################################################################
     * #                                /{id}                                  #
     * #########################################################################
     */
    @GetMapping(value = "/{id}", produces={MediaType.APPLICATION_JSON_VALUE})
    ResponseEntity<Etudiant> getOneStudent(@PathVariable int id) {
        Optional<Etudiant> query_result = this.etudiantRepository.findById(id);

        if(query_result.isPresent()) return ResponseEntity.ok(query_result.get());

        return ResponseEntity.notFound().build();
    }

    @PutMapping(value="/{id}", produces={MediaType.APPLICATION_JSON_VALUE})
    ResponseEntity<Etudiant> updateCompleteStudent(@PathVariable int id, @RequestBody Etudiant newStudent) {

        Optional<Etudiant> queryResult = this.etudiantRepository.findById(id);

        if(queryResult.isPresent()) {
            Etudiant student = queryResult.get();

            student = newStudent;
            student.setId(id);

            this.etudiantRepository.save(student);

            return ResponseEntity.ok(student);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PatchMapping(value="/{id}", produces={MediaType.APPLICATION_JSON_VALUE})
    ResponseEntity<Etudiant> updatePartiallyStudent(@PathVariable int id, @RequestBody Map<String, Object> updates) {

        Optional<Etudiant> queryResult = this.etudiantRepository.findById(id);

        if(queryResult.isPresent()) {
            Etudiant student = queryResult.get();

            for(Map.Entry<String, Object> entry: updates.entrySet()) {
                try {
                    Method setter = new PropertyDescriptor(entry.getKey(), Etudiant.class).getWriteMethod();
                    setter.invoke(student, entry.getValue());
                }catch(Exception e) {
                    System.err.println(e);
                }

            }

            this.etudiantRepository.save(student);

            return ResponseEntity.ok(student);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping(value = "/{id}", produces={MediaType.APPLICATION_JSON_VALUE})
    void deleteStudent(@PathVariable int id) {
        this.etudiantRepository.deleteById(id);
    }

    /* #########################################################################
     * #                                /select                                 #
     * #########################################################################
     */
    @GetMapping(value = "/select", produces={MediaType.APPLICATION_JSON_VALUE})
    List<Etudiant> select(@RequestParam String groupe) {
        List<Etudiant> students = new ArrayList<>();

        this.etudiantRepository.findAllByGroupe(groupe).forEach(students::add);

        return students;
    }

}
