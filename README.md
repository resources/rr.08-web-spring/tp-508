# R5.09 - TP 508

## Accessing H2 DB

Add this in `application.properties`
```
spring.h2.console.enabled=true
```

[http://localhost:8080/h2-console](http://localhost:8080/h2-console)

